# PokemonsAssignment3

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.3.

## Table of Contents
- Background
- Usage
- Development server
- Code scaffolding
- Build
- Running unit tests
- Running end-to-end tests
- Further help

## Background
Pokemon Trainer is an Angular web application that allows a user to collect Pokemons received from the [PokeAPI](https://pokeapi.co/?ref=public-apis). It was made during an assignment on Noroff. 
The application consists of a landing page where the users must log in by entering their username. From here they will be redirected to the Pokemon Catalogue page, where all the available pokemons are listed. The user can catch pokemons by clicking the open pokeball that is shown next to each pokemon. When catched, the pokeball will be closed. The user can also click the closed pokeball to release the pokemon. 
All the collected pokemons will be saved in a Trainer API and displayed on the trainer-page. Here, the user also has the ability to remove a pokemon from the collection by clicking a delete-icon. 
The application also consists of a navbar that can be used to navigate between the pokemon catalogue page and the trainer-page. The navbar also contains a button for logging out. 


## Usage
To open the application click on [Pokemon Trainer]( https://tlp-assignment3-angular.herokuapp.com/login), and the application will run in your browser. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

