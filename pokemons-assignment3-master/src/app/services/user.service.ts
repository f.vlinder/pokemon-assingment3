import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';
import { Pokemon } from '../models/pokemon.model';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _user: User | undefined;

  get user(): User | undefined {
    return this._user;
  }
  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!); // user! = never be undefined
    this._user = user;
  }

  removeUser() {
    StorageUtil.removeItem(StorageKeys.User);
    this._user = undefined;
  }

  constructor() {
    this._user = StorageUtil.storageRead<User>(StorageKeys.User);
  }

  public hasCatched(pokemonId: number): boolean {
    if (this._user) {
      return Boolean(
        this.user?.pokemon.find((pokemon: Pokemon) => pokemon.id === pokemonId)
      );
    }
    return false;
  }

  public releasePokemon(pokemonId: number): void {
    if (this._user) {
      this._user.pokemon = this._user?.pokemon.filter(
        (pokemon: Pokemon) => pokemon.id !== pokemonId
      );
    }
  }

  public catchPokemon(pokemon: Pokemon): void {
    if (this._user) {
      this._user.pokemon.push(pokemon);
    }
  }
}
