import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  //dependency injection: injects http client to our service
  constructor(private readonly http: HttpClient) {}

  //models, httpclient, observables, and RxJS operators
  // LOGIN : we need to login
  public login(username: string): Observable<User> {
    return this.checkUsername(username).pipe(
      switchMap((user: User | undefined) => {
        if (user === undefined) {
          //user does not exists
          return this.createUser(username);
        }
        return of(user);
      })
    );
  }

  //CHECK if user exist
  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`).pipe(
      //RxJS Operators
      map((response: User[]) => response.pop())
    );
  }

  //IF NOT - create a user
  private createUser(username: string): Observable<User> {
    // create user
    const user = {
      username,
      pokemon: [],
    };
    //headers -> API Key
    const headers = new HttpHeaders({
      'Content-Type': 'Application/json',
      'x-api-key': apiKey,
    });
    //POST -> create items in the server
    return this.http.post<User>(apiUsers, user, { headers });
  }
}
