import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';
const { apiKey, apiUsers } = environment;
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { Observable, tap } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class CatchPokemonService {
  constructor(
    private readonly pokemonService: PokemonCatalogueService,
    private readonly userService: UserService,
    private http: HttpClient
  ) {}

  //get the pokemon based on the id

  //patch request to our server with the userid and the pokemon

  public catchPokemons(pokemonId: number): Observable<User> {
    if (!this.userService.user) {
      throw new Error('catchPokemon: There is no user');
    }
    const user: User = this.userService.user;

    //find the pokemon
    const pokemon: Pokemon | undefined =
      this.pokemonService.pokemonById(pokemonId);

    if (!pokemon) {
      throw new Error(
        'catchPokemon: There is no pokemon with id: ' + pokemonId
      );
    }
    //if this has already been catched, release pokemon
    if (this.userService.hasCatched(pokemonId)) {
      this.userService.releasePokemon(pokemonId);
    } else {
      this.userService.catchPokemon(pokemon);
    }

    const headers = new HttpHeaders({
      'Content-Type': 'Application/json',
      'x-api-key': apiKey,
    });

    //upadte(patch) pokemon to catched pokemons
    return this.http
      .patch<User>(
        `${apiUsers}/${user.id}`,
        {
          pokemon: [...user.pokemon], //aldready updated
        },
        { headers }
      ) //when susccesfull update server
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        })
      );
  }
}
