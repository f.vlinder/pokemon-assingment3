import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ThisReceiver } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { finalize, map, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';

const { apiPokemons } = environment;

const { apiPokemonPictures } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonCatalogueService {
  private _pokemons: Pokemon[] = [];
  private _error: string = '';
  private _loading: boolean = false;
  pokemon: Pokemon[] = [];
  onePokemon?: Pokemon;
  idFromUrl: number = 0;

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }
  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) {}

  public pokemonById(id: number): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id);
  }

  public getPokemon(): void {
    if (this._pokemons.length > 0 || this.loading) {
      return;
    }
    this.http.get<PokemonResponse>(apiPokemons).subscribe({
      next: (pokemonResponse: PokemonResponse) => {
        pokemonResponse.results.map((i) => {
          (this.idFromUrl = Number(i.url.split('/').filter(Boolean).pop())),
            this._pokemons.push(
              (this.onePokemon = {
                ...i,
                id: this.idFromUrl,
                imageUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/${this.idFromUrl}.gif`, //apiPokemonPictures + `${this.idFromUrl}.png`,
              })
            );
          //pushing to array twice after reloading page, here we remove any duplicates with the same name

          const uniqueValues = new Set(this._pokemons.map((v) => v.name));

          if (uniqueValues.size < this._pokemons.length) {
            this._pokemons.pop();
          }
        });
      },
    });
  }
}
