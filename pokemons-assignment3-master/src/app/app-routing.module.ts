import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { RouteConfigLoadStart, RouterModule } from '@angular/router';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { ProfilePage } from './pages/profile/profile.page';
import { LoginPage } from './pages/login/login.page';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login', //we wan to have login on the first page
  },
  {
    path: 'login',
    component: LoginPage,
    canActivate: [LoginGuard],
  },
  {
    path: 'pokemons',
    component: PokemonCataloguePage,
    canActivate: [AuthGuard], //not allowing th euser to access path without login
  },
  {
    path: 'profile',
    component: ProfilePage,
    canActivate: [AuthGuard], //not allowing th euser to access path without login
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], //import modules
  exports: [RouterModule], // expose module and its features
})
export class AppRoutingModule {}
