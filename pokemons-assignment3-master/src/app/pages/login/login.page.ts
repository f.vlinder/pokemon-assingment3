import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

//  ng g c ./pages/profile --type=page

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage implements OnInit {
  constructor(private readonly router: Router) {}

  handleLogin(): void {
    this.router.navigateByUrl('/pokemons');
  }

  ngOnInit(): void {}
}
