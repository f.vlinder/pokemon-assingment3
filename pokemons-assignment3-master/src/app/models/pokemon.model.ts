// Getting info from pokemon 1: https://pokeapi.co/api/v2/pokemon/1

export interface Pokemon {
  id: number;
  name: string;
  url: string;
  imageUrl: string;
}

export interface PokemonResponse {
  results: Pokemon[];
}
