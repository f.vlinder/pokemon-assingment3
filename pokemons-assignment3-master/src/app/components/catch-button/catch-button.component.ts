import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { CatchPokemonService } from 'src/app/services/catch-pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css'],
})
export class CatchButtonComponent implements OnInit {
  constructor(
    private readonly pokemonCatchService: CatchPokemonService,
    private readonly userService: UserService
  ) {}
  public loading: boolean = false;
  public isCatched: boolean = false;

  @Input() pokemonId: number = 0;

  ngOnInit(): void {
    //inputs are resolved!
    this.isCatched = this.userService.hasCatched(this.pokemonId);
  }

  onCatchClick(): void {
    this.loading = true;
    //add pokemons to catched pokemons
    this.pokemonCatchService.catchPokemons(this.pokemonId).subscribe({
      next: (response: User) => {
        this.loading = false;
        this.isCatched = this.userService.hasCatched(this.pokemonId);
      },
      error: (error: HttpErrorResponse) => {
        console.log(error.message);
      },
    });
  }
}
