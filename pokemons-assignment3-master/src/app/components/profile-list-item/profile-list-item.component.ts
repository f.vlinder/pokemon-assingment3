import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { CatchPokemonService } from 'src/app/services/catch-pokemon.service';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile-list-item',
  templateUrl: './profile-list-item.component.html',
  styleUrls: ['./profile-list-item.component.css']
})
export class ProfileListItemComponent implements OnInit {
  @Input() pokemon?: Pokemon;

  constructor(
    private readonly userService: UserService
    ) { }

  ngOnInit(): void {
  }

  public onRemovePokemonClick(pokemonId: number):void{
    this.userService.releasePokemon(pokemonId);
  }
}


