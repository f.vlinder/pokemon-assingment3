import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { LoginService } from 'src/app/services/login.service';
import { NgForm } from '@angular/forms';
import { StorageUtil } from 'src/app/utils/storage.util';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  get user(): User | undefined {
    return this.userService.user;
  }

  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!); // user! = never be undefined
    this.userService.user = undefined;
  }

  @Output() logout: EventEmitter<void> = new EventEmitter();

  constructor(
    private readonly userService: UserService,
    private readonly loginService: LoginService,
    private readonly router: Router
  ) {}

  logoutSubmit(): void {
    this.userService.removeUser();

    this.router.navigateByUrl('/login');
  }
  ngOnInit(): void {}
}
