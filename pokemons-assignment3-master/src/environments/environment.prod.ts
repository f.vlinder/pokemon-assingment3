export const environment = {
  production: true,
  apiUsers: 'https://react-signlanguage-api.herokuapp.com/trainers',
  apiPokemons: 'https://pokeapi.co/api/v2/pokemon/', // must define pokemon id at the end /pokemon/19 (for rattata)
  apiPokemonPictures:
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/', // must define png at the end /pokemon/19.png (for rattata)
  apiKey: 'TLoDjrKkH96dJyLzmTkfrAo1gVAFkezHTmB99XXAUNDSeNa5Fw516N19StW4Naxt',
};
